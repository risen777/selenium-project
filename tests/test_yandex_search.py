import allure
import selenium
from allure_commons.types import Severity
from selenium.webdriver.chrome import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager import driver

@allure.title('More than 10 results')
@allure.severity(Severity.CRITICAL)
def test_yandex_search():
    from selenium import webdriver
    from webdriver_manager.chrome import ChromeDriverManager
    driver = webdriver.Chrome(ChromeDriverManager().install())
    # driver = selenium.webdriver.Chrome(executable_path= "../chromedriver")
    with allure.step('Open the page of search'):
      driver.get("https://ya.ru")

    with allure.step('Search the marker.yandex.ru'):
     search_input= driver.find_element_by_xpath('//input[@id="text"]')
     search_button= driver.find_element_by_xpath('//div[@class="search2__button"]')
     search_input.send_keys('market.yandex.ru')
     search_button.click()

    def check_results_count(driver):
       inner_search_results = driver.find_elements_by_xpath('//li[@class="serp-item"]')
       return len(inner_search_results) >= 10

    with allure.step('Expect that the number of results is more than 10'):
     WebDriverWait(driver, 5, 0.5).until(check_results_count, 'The number of results is more than 10')
     search_results = driver.find_elements_by_xpath('//li[@class="serp-item"]')
     search_results[0].find_element_by_xpath('.//h2/a').click()

    with allure.step('Open the new page'):
     driver.switch_to_window(driver.window_handles[1])

    with allure.step('Check the title of page'):
     assert driver.title == "Яндекс.Маркет — выбор и покупка товаров из проверенных интернет-магазинов"

    driver.quit()